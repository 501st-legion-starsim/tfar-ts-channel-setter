param ([switch]$verbose = $false, [switch]$publish = $false)
if($verbose -eq $true){$VerbosePreference = "Continue"}else{$VerbosePreference = "SilentlyContinue"}

$comment = '// 501st TFAR Channel Setting, Automatically Generated'
$str1 = 'force force TFAR_Teamspeak_Channel_Name = "TFR:'
$str2 = '";'
$addonBuilder = Join-Path (Get-ItemProperty "HKCU:\Software\Bohemia Interactive\Arma 3 Tools").path "AddonBuilder\AddonBuilder.exe"
$publisherCmd = Join-Path (Get-ItemProperty "HKCU:\Software\Bohemia Interactive\Arma 3 Tools").path "Publisher\PublisherCmd.exe"
$WorkshopIDs = @(
    "3038260782",#Server1
    "3038262791",#Server2
    "3038263253",#Server3
    "3038263926",#Server4
    "3038264454",#Server5
    "3038264797",#Server6
    "3038265340",#Server7
    "3038265607",#Server8
    "3038265991",#Server9
    "3038266328",#Server10
    "3038266908",#Server11
    "3038272411",#Server12
    "3038273013" #Server13
)
$i = 0
foreach($id in $WorkshopIDs)
{
    $i++
    if($id -eq "0"){continue;} #skip unpublished mods

    #remove the output folder if it exists
    if(Test-Path -Path ".\Output\@501st_cba_settings_$i") {
        Write-Verbose "Old Mod $i Found, Removing."
        Remove-Item -Recurse -Force -Confirm:$false ".\Output\@501st_cba_settings_$i"
        #if it exists and we fail to delete it, its in use, exit.
        if(Test-Path -Path ".\Output\@501st_cba_settings_$i")
        {
            Write-Error "Failed to delete old mod, in use?"
            Exit
        }
    }

    if(Test-Path ".\Settings\$i\cba_settings.sqf")
    {
        Write-Verbose "$i Custom Config"
        $settings = Get-Content ".\Settings\$i\cba_settings.sqf" -Raw
    }else{
        Write-Verbose "$i Default Config"
        $settings = Get-Content ".\Settings\Default\cba_settings.sqf" -Raw
    }

    #build settings file and write out.
    "$settings`n`n$comment`n$str1$i$str2" > ".\addons\cba_settings_userconfig\cba_settings.sqf"
    #create output folder
    New-Item -Path ".\Output\" -Name "@501st_cba_settings_$i" -ItemType "directory"
    #build addon
    & "$addonBuilder" "$(Get-Location)\addons\cba_settings_userconfig" "$(Get-Location)\Output\@501st_cba_settings_$i\addons\cba_settings_userconfig.pbo" -prefix="cba_settings_userconfig" -clear -include="include.txt"
    #move addon to correct place and clean up
    Rename-Item ".\Output\@501st_cba_settings_$i\addons\cba_settings_userconfig.pbo" "out"
    Move-Item ".\Output\@501st_cba_settings_$i\addons\out\cba_settings_userconfig.pbo" ".\Output\@501st_cba_settings_$i\addons\"
    Remove-Item ".\Output\@501st_cba_settings_$i\addons\out"   

    #Publish Addon
    if($publish -eq $true){
        & $publisherCmd update /id:$id /changenote:"See: https://gitlab.com/501st-legion-starsim/cba-settings" /path:"$(Get-Location)\Output\@501st_cba_settings_$i"
    }
}

#puts all the addons into a zip file
Compress-Archive -Path ".\Output\@*" -DestinationPath ".\501st-all-settings.zip" -Force
